#include "../include/io.h"
enum init_status init_path(int argc, char **argv, char **in_path, char **out_path){
    if (argc != 3){ 
        return INVALID_PARAMETERS_ARGUMENTS;
    }
    *in_path = argv[1]; 
    *out_path = argv[2]; 
    return 0;
}

enum open_status open_file(char **in_path, char **out_path, FILE **in_file, FILE **out_file){
    *in_file = fopen(*in_path, "rb"); 
    if (*in_file == NULL){
        return FAILED_TO_OPEN_IN_FILE; 
    }
    *out_file = fopen(*out_path, "wb"); 
    if (*out_file == NULL){
        return FAILED_TO_OPEN_OUT_FILE; 
    }
    return 0; 
}

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2){
    fclose(*in_file); 
    fclose(*out_file); 
    free(pic1->data); 
    free(pic2->data); 
    return 0; 
}
