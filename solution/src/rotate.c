#include "../include/bmp.h"

struct pixel* get_pixel(struct image img, uint64_t column, uint64_t row){
    return &img.data[img.width*row+column];
}
void set_pixel(struct pixel* rotated, struct pixel* to_rotate){
    *rotated = *to_rotate;
}

struct image rotate(struct image* to_rotate) {
    struct image rotated = {
        .width = (*to_rotate).height, 
        .height = (*to_rotate).width
    };
    rotated.data = malloc(sizeof(struct pixel) * (*to_rotate).width * (*to_rotate).height); 

    for (uint64_t j = 0; j < (*to_rotate).height; j++){  
        for (uint64_t i = 0; i < (*to_rotate).width; i++){ 
            set_pixel(get_pixel(rotated, rotated.width-j-1, i), get_pixel(*to_rotate, i, j));
        }
    }
    return rotated;
}
