#include <../include/bmp.h>
#include <../include/io.h>
#include <../include/rotate.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    char *in_path;
    char *out_path;
    FILE *in_file;
    FILE *out_file;

    switch (init_path(argc, argv, &in_path, &out_path)){
    case INVALID_PARAMETERS_ARGUMENTS:
        fprintf(stderr, "Invalid parameters arguments");
        return 1;
    case INIT_OK:
        printf("Path has been initialized!");
    }


    switch (open_file(&in_path, &out_path, &in_file, &out_file)){ 
        case FAILED_TO_OPEN_IN_FILE:
            fprintf(stderr, "Failed to open input file");
            return 1;
        case FAILED_TO_OPEN_OUT_FILE:
            fprintf(stderr, "Failed to open output file");
            return 1;
        case OPEN_OK:
            printf("Files have been opened!");  
    }

    struct image picture;

    enum read_status r_s = from_bmp(in_file, &picture);
    if(r_s != 0){ 
        fprintf(stderr, "Read error");
        return 1;
    }

    struct image rotated_picture = rotate(&picture); 

    enum write_status w_s = to_bmp(out_file, &rotated_picture); 
    if (w_s != 0) { 
        fprintf(stderr, "Write error");
        return 1;
    }

    close_file(&in_file, &out_file, &picture, &rotated_picture);

    return 0;
}
