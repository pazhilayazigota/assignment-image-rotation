#include "../include/bmp.h"

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

uint64_t calculate_padding(uint64_t width){
    return 4 - ((width * 3) % 4);
}

enum read_status init_header(FILE* in, struct bmp_header* header){
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_ERROR;
    }
    if ((*header).biSize != 40){ 
        return READ_INVALID_HEADER;
    }
    if ((*header).biBitCount != 24){ 
        return READ_INVALID_BITS;
    }
    if ((*header).bfType != 0x4D42) { 
         return READ_INVALID_SIGNATURE; 
    }
    return 0;
}

void create_img(struct image* img, struct bmp_header header){
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(sizeof(struct pixel)*img->width*img->height);
}

void deinit_img(struct image* img){
    free(img->data);
    img->height = 0;
    img->width = 0;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    if(!in || !img){ 
        return READ_ERROR;
    }

    struct bmp_header header;
    enum read_status init_header_status;

    if((init_header_status = init_header(in, &header))!=0){
        return init_header_status;
    }
    create_img(img, header);

    uint64_t const padding = calculate_padding(img->width);

    for (uint64_t i = 0; i < img->height; i++) { 
        if (fread(&img->data[i * img->width], 3, img->width, in) != img->width){
            deinit_img(img);
            return READ_ERROR;
        }
        if (fseek(in, (long) padding, SEEK_CUR) != 0){
            deinit_img(img);
            return READ_ERROR;
        }
    }
    return READ_OK; 
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if(!out || !img){ 
        return WRITE_ERROR;
    }

    uint64_t const padding = calculate_padding(img->width);

    struct bmp_header header ={
        .bfType=0x4D42,
        .biSizeImage=(img->width*3+padding)*img->height,
        .bOffBits=sizeof(struct bmp_header), 
        .biSize=40, 
        .biWidth=img->width, 
        .biHeight=img->height,
        .biPlanes=1,
        .biBitCount=24,
        .bfileSize=(img->width*3+padding)*img->height + sizeof(struct bmp_header)
    };
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(&img->data[i * img->width], 3, img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if (fseek(out, (long) padding, SEEK_CUR) != 0){
            return WRITE_ERROR;
        };
    }
    return WRITE_OK; 
}
