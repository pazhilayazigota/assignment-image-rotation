#ifndef IO_H
#define IO_H

#include "./bmp.h"

enum init_status {
    INIT_OK = 0,
    INVALID_PARAMETERS_ARGUMENTS
};

enum open_status {
    OPEN_OK = 0,
    FAILED_TO_OPEN_IN_FILE,
    FAILED_TO_OPEN_OUT_FILE
};

enum close_status {
    CLOSE_OK = 0
};

enum open_status open_file(char **in_path, char **out_path, FILE **in_file, FILE **out_file);

enum init_status init_path(int argc, char **argv, char **in_path, char **out_path);

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2);

#endif
